+++
title = "Minigames - Overview"
date = "2023-11-26T00:00:00-05:00"
author = "Reeve"
authorTwitter = "" #do not include @
cover = ""
tags = ["coding-project", "kotlin", "minecraft", "plugin", "spigot", "minigames"]
keywords = ["minecraft", "plugin", "minigames"]
description = "A work in progress project, including my planning process. This is an overview, and also includes any general setup of the project"
showFullContent = false
readingTime = true
hideComments = false
+++

# Minigames

If you have not read my other posts, this is a good place to start!
This post is the overview of a series of posts I'm working on, all based around making a Minecraft minigames server.
The general rule of thumb is that I'll be making most Minecraft components from scratch, aside from the usual Spigot and Velocity/(or BungeeCord?) server software.

If you're interested in understanding how multiple Minecraft servers can work together, check out the matchmaking post!