+++
title = "Minigames - One In The Quiver gamemode"
date = "2023-11-28T00:00:00-05:00"
author = "Reeve"
authorTwitter = "" #do not include @
cover = ""
tags = ["coding-project", "kotlin", "minecraft", "plugin", "spigot", "minigames"]
keywords = ["minecraft", "plugin", "minigames"]
description = "A work in progress project, including my planning process. The second part of a series on creating a minigame server."
showFullContent = false
readingTime = true
hideComments = false
+++

# What's One In The Quiver?

One in the quiver (OITQ) is an old Minecraft minigame that was one of the first ones I played consistently (aside from Hunger Games).
As far as I know, the game mode this is based on is the Call of Duty (One In The Chamber)[https://callofduty.fandom.com/wiki/One_in_the_Chamber].
The basic idea is that you have one bullet (or in this case an arrow) and if you miss, you need to use your knife.
There's some extra rules in the COD version, but we'll use these rules:

1. Each arrow hit is a K.O.
2. You have to hit **someone else**
3. Each K.O. is 1 point
4. Players will heal 3 hearts per K.O.
5. Players will get another arrow if they get a K.O.
6. Players start with a stone sword, a bow, and 1 arrow
7. Arrows cannot be picked up
8. Every 3 K.O.s is a full heal.
9. Fall damage is disabled
10. Players cannot break or place blocks
11. Players are spawned at random spawn points on the map, but filtered for ones that do not have another player nearby.
12. No animals or monsters will spawn

There's more we can add later, but for the basics that covers it.

## Project Setup

As is typical of a combat server, we will be using Minecraft 1.8.8, which is the version before the combat overhaul.
Next, I'm going to set up a parent project for any future minigame projects.
This will serve as a modular repo with all the different minigames in it, including a `base` module for common code.

So first up, we need to set up the parent project.
I'll be using Gradle with the Kotlin DSL, but it will most likely look similar with the Groovy DSL.

```kotlin
plugins {
    kotlin("jvm") version "1.9.20"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "dev.reeve"
version = "1.0-SNAPSHOT"

allprojects {
    repositories {
        mavenCentral()

        maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") {
            content {
                includeGroup("org.bukkit")
                includeGroup("org.spigotmc")
            }
        }

        maven("https://oss.sonatype.org/content/repositories/snapshots")
    }
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "com.github.johnrengelman.shadow")

    dependencies {
        testImplementation(kotlin("test"))

        compileOnly("org.spigotmc:spigot-api:1.8.8-R0.1-SNAPSHOT")
    }

    tasks.test {
        useJUnitPlatform()
    }

    kotlin {
        jvmToolchain(8)
    }
}
```

This does several things for us.
First, it adds the plugins we'll be using;
- Kotlin, which will be the language of choice for this project.
- Shadow, which will package our dependencies into our `jar`