+++
title = "Getting Started with DnD"
date = "2023-07-03T00:17:38-05:00"
author = "Reeve"
authorTwitter = "" #do not include @
cover = ""
tags = ["dnd"]
keywords = ["dnd"]
description = "Some tips I've picked up from learning how to play DnD myself"
showFullContent = false
readingTime = true
hideComments = false
+++

# What's DnD?

DnD stands for Dungeons and Dragons, and as of writing, the latest version is 5e.
It's a roleplaying game, where all you really need is your imagination, dice, and a sheet or two of paper.
However, it is a little intimidating to get started, since there's quite a bit to understand before you get started.
Technically, you could probably just learn as you go, but if you're like me you might want to understand as much as possible before you get started.
The main book where you'll find most of your information is the [Players Handbook](https://a.co/d/bYrKqXD), which will have all the core rules and information.
If (like me) you'd like to see all the content there is, there's quite a few other books to check out:

- [Dungeon Master's Guide](https://a.co/d/1jbeRPI) - This isn't something you'll really need to worry about as a player, but it has a lot more information
- [Monster Manual](https://a.co/d/7ZQzlmz) - This is more helpful for starting players, since it has information about a lot of the different monsters your party might run into (with pictures of them all)
- [Tasha's Cauldron of Everything](https://a.co/d/1p9VQks) - Adds the Artificer class, as well as additional optional rules for the game, and DM tools and resources
- [Xanathar's Guide to Everything](https://a.co/d/2L2aWBd) - More new rule options, as well as DM tools and resources

And there's quite a few more, but those are the ones I have so far.

# Important Reading

Although the entire Player's Handbook is really well written, and it's definitely something you should read through at some point, I think there's quite a bit you'll just figure out as you play -- and some might not even apply to your session.
Some of the chapters I found most important you can read some about below:

- Introduction
- Chapter 7: Using Ability Scores
- Chapter 8: Adventuring
- Chapter 9: Combat
- Chapter 10: Spellcasting - Only necessary if you're a spellcasting class (or subclass)
- Chapter 3: Classes -- once you choose one (more on that below)

# Creating a Character

So the class I've been learning about is called Artificer.
It's not in the original ruleset from the Player's Handbook, so a lot of my reading has been in Tasha's because of that.
I didn't really learn in the right order, but it has worked out and I'll tell you a little about it.

## Class

So the first order of business was picking out a class, which was pretty easy with the [DnD Beyond](https://www.dndbeyond.com) page for [classes](https://www.dndbeyond.com/classes).
Artificer immediately piqued my interest, so I didn't explore too much more, but what I'd do is to look through these and see if anything catches your eye.
From there (if it's part of the Player's Handbook), you can click and get more information, or just find it in the beginning of the book.
This will be something you might need to reference a lot as you're learning how your class works and what options you have.

Another thing to consider is Subclasses, which are unlocked at level 3 and bring some specialization with different features.
The Player's Handbook has some subclass options, and Tasha's and Xanathar's have even more for most (if not all) classes.

Eventually, there's also the Multiclassing feature, but I don't really understand how that works yet myself.
It's unlocked later on in the game (I think around level 10) so it's not anything you'll have to worry about for a while.

## Race

There's another helpful page on [DnD Beyond](https://www.dndbeyond.com) for [races](https://www.dndbeyond.com/races).
You can do these in any order that works for you, but I chose my race next.
It took a while and I changed a few times, but I ended up going with the Astral Elves.
These will have some specific bonuses like stats or special skills (better vision at night, etc.), but otherwise it's largely just whatever suits you.

## Backstory & Name

Once you have a general idea of the playstyle you want, you'll need to start bringing your character to life.
This process is largely whatever works for you, but there are some important details you'll want to come up with:

- Name
- Backstory
- Appearance (hair, height, weight, etc.)
- Age
- Bonds
- Goals
- Flaws
- And anything else you can think of to make this character feel like someone you can imagine and talk with

## Character Sheet

Once you have all these details figured out, you can start filling out a [Character Sheet](https://dnd.wizards.com/resources/character-sheets).
This will be your main place for all your character information, from your lore to your stats, inventory, and spells (assuming you're a spellcaster).
I'd go with the 5th edition sheets from that page, but there's also some ready-to-play sheets you can reference or use.
If you have the Player's Handbook, there's also a character sheet in the back of the book (though it isn't really tearable, you'd need to have the book open to use it)

{{< figure src="/images/dnd/zumlen_cs.png" caption="Depending on how invested you are or want to be, there are some special sheets people have made that are really well designed." >}}